## Summary:
<!-- Sumarize the feature request in one sentence. -->
### What is the Feature Request in Detail?:
<!-- Explain. -->
### Have you thought of alternative implementations?:
<!-- Explain. -->
### Will this impact other features?:
<!-- List the features it impacts and if able explian how. -->
### Mockups:
<!-- Please attach any images or mockups that you have made. -->
### Do you plan to start a PR for this feature?:
<!-- If not do you have an idea of how to go about coding the feature? -->
/label ~feature_request